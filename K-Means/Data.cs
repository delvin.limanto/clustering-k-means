﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_Means
{
    class Data
    {
        string nama, cluster;
        List<double> coordinate = new List<double>();

        public Data(string nama, double[] coor)
        {
            this.cluster = "";
            this.nama = nama;
            for(int i = 0;i < coor.Length; i++)
            {
                this.coordinate.Add(coor[i]);
            }
        }

        public string cetak()
        {
            string s = this.nama;
            for(int i = 0;i < this.coordinate.Count; i++)
            {
                s += " - " + this.coordinate[i];
            }
            return s;
        }

        public int getCoordinateLenght()
        {
            return this.coordinate.Count;
        }

        public double getCoordinate(int idx)
        {
            return this.coordinate[idx];
        }

        public List<double> getCoordinate()
        {
            return this.coordinate;
        }

        public void setCoordinate(int idx, double value)
        {
            this.coordinate[idx] = value;
        }

        public string getNama()
        {
            return this.nama;
        }

        public void setCluster(string str)
        {
            this.cluster = str;
        }

        public string getCluster()
        {
            return this.cluster;
        }
    }
}
