﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace K_Means
{
    public partial class Form1 : Form
    {
        int maxIter = 1000, currIter = 1, jmlCluster = 0, banyakKoordinat = 0;
        OpenFileDialog openFileDialog1;
        List<Data> dataList = new List<Data>();
        List<Cluster> clusterList = new List<Cluster>();
        Random random = new Random();
        bool done = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.jmlCluster = Convert.ToInt32(textBox1.Text);
            if (textBox2.Text != "")
            {
                this.maxIter = Convert.ToInt32(textBox2.Text);
            }
            string[] filelines = File.ReadAllLines(openFileDialog1.FileName);
            this.reset_value();
            for(int i = 0;i < filelines.Length; i++)
            {
                string[] line = filelines[i].Split(';');
                string name = line[0];
                double[] coor = new double[line.Length - 1];
                for(int j = 1;j < line.Length; j++)
                {
                    coor[j - 1] = Convert.ToInt32(line[j]);
                }
                this.dataList.Add(new Data(name, coor));
            }
            this.normalization();
            this.clustering();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = Directory.GetCurrentDirectory(),
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "txt",
                Filter = "txt files (*.txt)|*.txt",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                button1.Text = openFileDialog1.FileName;
            }
        }

        private void reset_value()
        {
            this.dataList = new List<Data>();
            this.clusterList = new List<Cluster>();
            this.currIter = 1;
            this.done = false;
            richTextBox1.Clear();
        }

        private void normalization()
        {
            this.banyakKoordinat = this.dataList[0].getCoordinateLenght();
            for(int i = 0; i < banyakKoordinat; i++)
            {
                double min = double.MaxValue;
                double max = 0;
                for(int j = 0;j < this.dataList.Count; j++)
                {
                    double c = this.dataList[j].getCoordinate(i) * 1.0;
                    if(c < min) min = c;
                    if(c > max) max = c;
                }
                for(int j = 0;j< this.dataList.Count; j++)
                {
                    double c = this.dataList[j].getCoordinate(i);
                    this.dataList[j].setCoordinate(i, this.getNormalizedNum(c, min, max));
                }
            }
        }

        private double getNormalizedNum(double val, double min, double max)
        {
            return ((val - min) * 1.0) / ((max - min) * 1.0);
        }

        private void clustering()
        {
            MessageBox.Show("Silahkan tunggu hingga muncul mbox done");
            this.setCluster();
            this.printInitialState();
            while(this.currIter <= this.maxIter && !done)
            {
                richTextBox1.Text += "Iterasi ke-" + this.currIter + "\n";
                richTextBox1.Text += "------------------------------------------------------------------------\n";
                for (int i = 0;i < this.dataList.Count; i++)
                {
                    double[] jarak = new double[this.jmlCluster];
                    double minJarak = double.MaxValue;
                    string namaCluster = "";
                    for(int j = 0; j < this.jmlCluster; j++)
                    {
                        jarak[j] = this.countDistance(this.dataList[i].getCoordinate(), this.clusterList[j].getCoordinate());
                        richTextBox1.Text += "Jarak " + this.dataList[i].getNama() + " dengan cluster " + this.clusterList[j].getNama() + " = " + jarak[j] + "\n";
                        if(jarak[j] < minJarak)
                        {
                            minJarak = jarak[j];
                            namaCluster = this.clusterList[j].getNama();
                        }
                    }
                    richTextBox1.Text += "Data " + this.dataList[i].getNama() + " masuk kedalam cluster " + namaCluster + "\n********************************************************\n";
                    this.dataList[i].setCluster(namaCluster);
                }
                richTextBox1.Text += "Koordinat cluster baru\n";
                this.calculateNewCoordinateForCluster();
                richTextBox1.Text += "------------------------------------------------------------------------\nAnggota cluster iterasi ke-" + this.currIter + "\n";
                this.printClusterMember();
                this.currIter++;
                richTextBox1.Text += "=====================================================\n";
            }
            richTextBox1.Text += "------------------------------------------------------------------------\nAnggota cluster final\n";
            this.printClusterMember();
            richTextBox1.Text += "\n Selesai pada iterasi ke-" + (this.currIter - 1);
            MessageBox.Show("Done");
        }

        private void printClusterMember()
        {
            for (int i = 0; i < this.clusterList.Count; i++)
            {
                string s = "Anggota cluster " + this.clusterList[i].getNama() + " = ";
                for (int j = 0; j < this.dataList.Count; j++)
                {
                    if (this.dataList[j].getCluster() == this.clusterList[i].getNama())
                    {
                        s += this.dataList[j].getNama() + ", ";
                    }
                }
                richTextBox1.Text += s + "\n";
            }
        }

        private void calculateNewCoordinateForCluster()
        {
            bool different = false;
            for(int i = 0;i < this.clusterList.Count; i++)
            {
                for(int j = 0;j < this.banyakKoordinat; j++)
                {
                    double jmlData = 0.0;
                    double total = 0.0;
                    for(int k = 0;k < this.dataList.Count; k++)
                    {
                        if(this.dataList[k].getCluster() == this.clusterList[i].getNama())
                        {
                            total += (this.dataList[k].getCoordinate(j) * 1.0);
                            jmlData++;
                        }
                    }
                    if(jmlData > 0)
                    {
                        total /= (jmlData * 1.0);
                        if (total != this.clusterList[i].getCoordinate(j))
                        {
                            different = true;
                        }
                        this.clusterList[i].setCoordinate(total, j);
                    }                    
                }
            }
            for (int i = 0; i < this.clusterList.Count; i++)
            {
                string s = this.clusterList[i].getNama();
                for (int j = 0; j < this.banyakKoordinat; j++)
                {
                    s += " - " + this.clusterList[i].getCoordinate(j);
                }
                richTextBox1.Text += s + "\n";
            }
            if (!different) this.done = true;
        }

        private double countDistance(List<double> dataCoordinate, List<double> clusterCoordinate)
        {
            double total = 0;
            for(int i = 0; i < this.banyakKoordinat; i++)
            {
                total += Math.Pow(dataCoordinate[i] - clusterCoordinate[i], 2) * 1.0;
            }
            return Math.Sqrt(total);
        }

        private void printInitialState()
        {
            richTextBox1.Text += "Initial State\n=====================================================\nDaftar cluster\n";
            for(int i = 0;i < this.clusterList.Count; i++)
            {
                string s = this.clusterList[i].getNama();
                for(int j = 0;j < this.banyakKoordinat; j++)
                {
                    s += " - " + this.clusterList[i].getCoordinate(j);
                }
                richTextBox1.Text += s + "\n";
            }
            richTextBox1.Text += "------------------------------------------------------------------------\nDaftar data (normalized, 0 - 1)\n";
            for (int i = 0; i < this.dataList.Count; i++)
            {
                richTextBox1.Text += this.dataList[i].cetak() + "\n";
            }
            richTextBox1.Text += "=====================================================\n";
        }

        private void setCluster()
        {
            List<int> taken = new List<int>();
            for(int i = 0;i < this.jmlCluster; i++)
            {
                double[] coor = new double[this.banyakKoordinat];
                //for(int j = 0; j < this.banyakKoordinat; j++)
                //{
                //    coor[j] = this.getRandomCoordinate();
                //}
                int idx = 0;
                bool take = false;
                do
                {
                    idx = this.random.Next(0, this.dataList.Count);
                    take = false;
                    for(int j = 0;j < taken.Count; j++)
                    {
                        if(taken[j] == idx)
                        {
                            take = true;
                        }
                    }
                } while (take);
                taken.Add(idx);
                List<double> temp = this.dataList[idx].getCoordinate();
                for(int j = 0;j < temp.Count; j++)
                {
                    coor[j] = temp[j];
                }
                this.clusterList.Add(new Cluster("C" + (i + 1), coor));
            }
        }

        private double getRandomCoordinate()
        {
            return this.random.NextDouble();
        }
    }
}
